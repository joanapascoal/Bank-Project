from django.test import TestCase
from rest_framework.test import *
from django.urls import reverse
from .views import *
from django.contrib.auth.models import User

class LoanModelTestCase(TestCase):

    def setUp(self):
        """Define the test client and other test variables."""

        user = User.objects.create(username="Here")
        self.client = APIClient()
        self.client.force_authenticate(user=user)

        self.amount = 2000
        self.term = 12
        self.rate = 0.2

        self.loan = Loan(amount=self.amount, term=self.term, rate=self.rate)

    def test_model_can_create_a_loan(self):
        """Test the loan model can create a loan."""
        old_count = Loan.objects.count()
        self.loan.save()
        new_count = Loan.objects.count()
        self.assertNotEqual(old_count, new_count)



class LoanViewTestCase(TestCase):
    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        user = User.objects.create(username="Peter")
        self.client.force_authenticate(user=user)
        self.loan_data = {'amount': '400', 'term': '12', 'rate': '0.02'}
        self.response = self.client.post(
            reverse('create'),
            self.loan_data,
            format="json")

    def test_api_can_create_a_loan(self):
        """Test the api has loan creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_authorization_is_enforced(self):
        """Test that the api has user authorization."""
        new_client = APIClient()
        res = new_client.get('/loans/', kwargs={'pk': 3}, format="json")
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_api_can_get_a_loan(self):
        """Test the api can get a given loan."""
        loan = Loan.objects.get()
        response = self.client.get(
            reverse('details',
                    kwargs={'pk': loan.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, loan.id)

    def test_api_can_update_loan(self):
        """Test the api can update a given loan."""
        loan = Loan.objects.get()
        change_loan = {'amount': '520', 'term': '11', 'rate': '0.02'}
        res = self.client.put(
            reverse('details', kwargs={'pk': loan.id}),
            change_loan, format='json'
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_loan(self):
        """Test the api can delete a loan."""
        loan = Loan.objects.get()
        print(loan)
        response = self.client.delete(
            reverse('details', kwargs={'pk': loan.id}),
            format='json',
            follow=True)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, loan.id)


class PaymentModelTestCase(TestCase):
    def setUp(self):
        self.loan = Loan(amount=2000, term=12, rate=0.2)
        self.loan.save()
        self.paymentType = 'made'
        self.date = '2018-09-05 02:18Z'
        self.amount = 50
        self.payment = Payment(paymentType=self.paymentType, date=self.date, amount=self.amount, loan=self.loan)


    def test_model_can_create_a_payment(self):
        """Test the loan model can create a loan."""
        old_count = Payment.objects.count()
        self.payment.save()
        new_count = Payment.objects.count()
        self.assertNotEqual(old_count, new_count)


class PaymentViewTestCase(TestCase):
    def setUp(self):
        """Define the test client and other test variables."""
        user = User.objects.create(username="Here")
        self.client = APIClient()
        self.client.force_authenticate(user=user)
        loan = Loan(amount=2000, term=12, rate=0.2)
        loan.save()
        self.payment_data = {'paymentType' : 'made', 'date':'2018-09-05 02:18Z', 'amount':52.01}
        self.response = self.client.post(
            '/loans/'+str(loan.id)+'/payment/',
            self.payment_data,
            format="json")
#
    def test_api_can_create_a_payment(self):
        """Test the api has payment creation capability."""
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_a_payment(self):
        """Test the api can get a given payment."""
        payment = Payment.objects.get()
        response = self.client.get(
            reverse('detailspayment',
                    kwargs={'pk': payment.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, payment.id)


