from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *
from django.views.generic import RedirectView

urlpatterns = {
    url(r'^$', RedirectView.as_view(url='/loans/')),
    url(r'^loans/$', CreateLoanView.as_view(), name="create"),
    url(r'^loans/(?P<pk>[0-9]+)/payment/$', CreatePaymentView.as_view(), name="createpayment"),
    url(r'^loans/(?P<pk>[0-9]+)/$', MixedLoanView.as_view(), name="details"),
    url(r'^loans/(?P<pk>[0-9]+)/balance/$', CalculateBalanceView.as_view(), name="balance"),
    url(r'^loans/list/$', GetAllLoanView.as_view(), name="listloans"),
    url(r'^payments/list/$', GetAllPaymentView.as_view(), name="listpayments"),
    url(r'^payments/(?P<pk>[0-9]+)/$', MixedPaymentView.as_view(), name="detailspayment"),
}

urlpatterns = format_suffix_patterns(urlpatterns)