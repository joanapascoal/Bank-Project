from rest_framework import serializers
from .models import *
from datetime import datetime


class LoanSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Loan
        fields = ('amount', 'term', 'rate', 'date_created')

    def to_representation(self, instance):
        r = float(instance.rate / 12)
        identifiers = dict()
        identifiers['installment'] = round((r + r/((1+r) ** instance.term - 1)) * float(instance.amount),2)
        representation = {
            'loan_id':  instance.id,
            'installment' : identifiers['installment'],
        }
        return representation


class LoanSerializerAll(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Loan
        fields = ('id', 'amount', 'term', 'rate', 'date_created')

    def to_representation(self, instance):
        r = float(instance.rate / 12)
        identifiers = dict()
        identifiers['installment'] = round((r + r / ((1 + r) ** instance.term - 1)) * float(instance.amount), 2)
        representation = {
            'loan_id': instance.id,
            'installment': identifiers['installment'],
            'amount' : instance.amount
        }
        return representation


class PaymentSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Payment
        fields = ('id', 'paymentType', 'date', 'amount')

    def to_representation(self, instance):
        representation = {
            'payment_id': instance.id,
            'paymentType': instance.paymentType,
            'date': instance.date,
            'amount': instance.amount,
            'loan_id': instance.loan.id,
        }
        return representation


class BalanceSerializer(serializers.Serializer):
    date = serializers.DateTimeField(default=datetime.now)

    class Meta:
        lookup_field = 'date'


