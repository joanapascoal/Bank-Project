from rest_framework import status , generics , mixins, permissions
from .serializers import *
from rest_framework.response import Response
from .models import *
from django.db.models import Sum
import sys, dateutil.parser

# Loan Section
class CreateLoanView(generics.CreateAPIView):
    """Create a Loan. Return loan_id and installment"""
    queryset = Loan.objects.all()
    serializer_class = LoanSerializer
    # permission_classes = IsAuthenticated

    def perform_create(self, serializer):
        """Save the post data when creating a new Loan."""
        serializer.save()

class MixedLoanView(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView):

    queryset = Loan.objects.all()
    serializer_class = LoanSerializerAll
    # permission_classes = IsAuthenticated

    def get(self, request, *args, **kwargs):
        """Get a Loan"""
        try:
            return self.retrieve(request, *args, **kwargs)
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        """Update a Loan"""
        try:
            return self.update(request, *args, **kwargs)
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        """Delete a Loan"""
        try:
            self.destroy(request, *args, **kwargs)
            return Response(status=200, data='Loan successful deleted')
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)

class GetAllLoanView(generics.ListAPIView):
        """Return a list of saved Loans"""
        queryset = Loan.objects.all()
        serializer_class = LoanSerializerAll



#Payment Section
class CreatePaymentView(generics.CreateAPIView):
    """Creates a record of a payment made or missed.
        payment: type of payment: made or missed.
        date: payment date.
        amount: amount of the payment made or missed in dollars.
    """
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = (permissions.IsAuthenticated)

    def perform_create(self, serializer):
        try:
            loan_id = self.kwargs['pk']
            """Save the post data when creating a new Payment."""
            this_loan = Loan.objects.get(id=loan_id)
            serializer.save(loan=this_loan)
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)

class GetAllPaymentView(generics.ListAPIView):
    """Return a list of saved Payments"""
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = (permissions.IsAuthenticated)

class MixedPaymentView(generics.RetrieveAPIView):

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    # permission_classes = (permissions.IsAuthenticated)

    def get(self, request, *args, **kwargs):
        """Get a Payment"""
        try:
            return self.retrieve(request, *args, **kwargs)
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)


#Balance section
class CalculateBalanceView(generics.CreateAPIView):
    serializer_class = BalanceSerializer
    # permission_classes = (permissions.IsAuthenticated)
    queryset = {}

    def post(self, request, *args, **kwargs):
        """Calculates Balance of a loan until some given date"""
        try:
            loan_id = self.kwargs['pk']
            posted_data = self.request.data
            this_loan = Loan.objects.get(id=loan_id)
            date = dateutil.parser.parse(posted_data['date'])
            balance = getBalance(this_loan, date)
            return_data = [
                {"balance": balance}
            ]
            return Response(status=200, data=return_data)
        except:
            exception_type, exception_value = sys.exc_info()[0:2]
            error = exception_type.__name__ + ': ' + str(exception_value)
            return Response(str(error), status=status.HTTP_400_BAD_REQUEST)


def getBalance(this_loan, date):
    try:
        loan_amount = this_loan.amount
        total_payed = Payment.objects.filter(loan=this_loan, paymentType='made', date__lte=date).aggregate(
            Sum('amount')).get('amount__sum', 0.00)
        balance = loan_amount - total_payed
        return balance
    except Exception as e:
        raise e
