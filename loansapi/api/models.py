from django.db import models
from datetime import datetime
from decimal import Decimal
from django.core.validators import MinValueValidator


class Loan(models.Model):
    """This class represents the Loan model."""
    amount = models.DecimalField(max_digits=10, decimal_places=2,  validators=[MinValueValidator(Decimal('0.01'))])
    term = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    rate = models.DecimalField(max_digits=10, auto_created=True, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    date_created = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        """Return a representation of the model instance."""
        return "%s %s %s %s" % (self.amount, self.term, self.rate, self.date_created)

class Payment(models.Model):
    """This class represents the Loan model."""
    madeormissed = (
        ('made', 'made'),
        ('missed', 'missed'),
    )
    paymentType = models.CharField(max_length=500, choices=madeormissed)
    date = models.DateTimeField(default=datetime.now)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE)

    def __str__(self):
        """Return a representation of the model instance."""
        return "%s %s %s %s" % (self.paymentType, self.date, self.amount, self.loan.id)
