# Bank Project

## Getting Started

Simulates a bank application to manage new loans and find out what the value and volume of outstanding debt are.

### Installing

Clone de Repository
```
git clone git@gitlab.com:joanapascoal/Bank-Project.git
```

Create a Virtual Environment
```
virtualenv env  (windows)
```

Install requirements
```
pip install -r requirements.txt
```

Activate virtual environment

```
.\env\Scripts\activate (windows)

 source venv/bin/activate (linux)
```

Move into the loansapi directory

```
cd loansapi
```

Run the server

```
python manage.py runserver
```

Open the browser 

```
http://127.0.0.1:8000
```

Login with superuser

```
username: owner
password: @123456+
```

## Using API without Client-Side with Token authetication

Make the HTTP requests on [Postman](https://www.getpostman.com/)

```
http://127.0.0.1:8000/get-token/
```

 Obtain that token

![alt text](loansapi/images/postman_1.jpg "print_postman1")

Use obtained token in Authorization header

![alt text](loansapi/images/postman_2.jpg "print_postman2")

Call a Endpoint
![alt text](loansapi/images/postman_3.jpg "print_postman3")

## Endpoints

#### POST /get-token/

##### Summary

Creates a token

* Payload Example
```
{
    'username' : owner,
    'password': @123456+
}
```
* Reply Example
```
{
    'token' : '3694864bba0761cf14d20658f9e3873bd91e19fc'
}
```


#### POST /loans/

##### Summary

Creates a loan application.

* Payload Example
```
{
	"amount": 1000,
	"term": 12,
	"rate": 0.05,
	"date": "2017-08-05 02:18Z",
}
```
* Reply Example
```
{
   {
	"loan_id": "125",
	"installment": 85.60
    }
}
```


#### PUT/GET/DELETE /loans/{id}

##### Summary

Update, Get and Delete by Loan Id

* PUT Payload Example 
```
{
	"amount": 1000,
	"term": 12,
	"rate": 0.05,
	"date": "2017-08-05 02:18Z",
}
```
* Reply Example
```
{
   {
    "loan_id": 2,
    "installment": 85.60,
    "amount": 10000
    }
}
```

#### GET /loans/list/

##### Summary

Return list of loans on DB

* Reply Example 
```
[
    {
        "loan_id": 1,
        "installment": 5.01,
        "amount": 10
    },
    {
        "loan_id": 2,
        "installment": 856.07,
        "amount": 10000
    }
]
```

#### POST /loans/{id}/payment/

##### Summary

Receive a Loan id as param and creates a payment.

* Payload Example
```
{
  "paymentType": "made",
  "date": "2017-09-05 02:18Z",
  "amount": 85.60,
}
```
* Reply Example
```
{
   {
    "payment_id": 1,
    "paymentType": "made",
    "date": "2017-09-05T02:18:00Z",
    "amount": 85.6,
    "loan_id": 2
    }
}
```

#### GET /payments/{id}/

##### Summary

Return information about param id.

* Reply Example 
```
{
    "payment_id": 1,
    "paymentType": "made",
    "date": "2017-09-05T02:18:00Z",
    "amount": 85.6,
    "loan_id": 2
}
```

#### GET /payments/list/

##### Summary

Return list of payments on DB

* Reply Example 
```
[
    {
        "loan_id": 1,
        "installment": 5.01,
        "amount": 10
    },
    {
        "loan_id": 2,
        "installment": 856.07,
        "amount": 10000
    }
]
```

#### POST /loans/{id}/balance/

##### Summary

Receive a Loan id as param and return volume of outstanding debt at some point in time.

* Payload Example
```
{
  "date": "2017-09-05 02:18Z",
}
```
* Reply Example
```
[
    {
        "balance": 9914.4
    }
]
```


## Database

To create a new superuser
```
python manage.py createsuperuser
```

If you change Models, apply:
```
python manage.py makemigrations
```
And then
```
python manage.py migrate
```


## Running the tests

Run the automated tests 

```
python manage.py test
```

## Author

* **Joana Pascoal** - [Linkedin](https://www.linkedin.com/in/joanapascoal) - [GitHub](https://github.com/joanapascoal)
